# fetch_exchange

A python module which users the fixer.io API to fetch and compare exchange values against a given currency.
* fixer.io API key required (sign up for free)


# usage
# import
from fetch_exchange import fetch_exchange


# call function with currency and base currencies
fetch_exchange("ZAR", ["USD", "EUR", "BTC"])


# Tkinter Version (Desktop App)
1. Add your fixer.io API key
2. Open the app with python3
